{{/* Usage {{ include "apitable.nameBuilder" (list ." 3 "b" 4) }} */}}
{{- define "apitable.nameBuilder" -}}
{{- prepend (rest .) (include "common.fullname" (first .)) | join "-" -}}
{{- end }}

{{- define "apitable.shared.probes" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $root := (get $.Values $label) }}
livenessProbe:
  tcpSocket:
    port: {{ $root.service.port }}
  initialDelaySeconds: 60
  periodSeconds: 60
startupProbe:
  tcpSocket:
    port: {{ $root.service.port }}
  initialDelaySeconds: 60
  periodSeconds: 60
{{- end }}

{{- define "apitable.shared.image" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $root := (get $.Values $label) }}
securityContext:
  {{- toYaml $root.securityContext | nindent 2 }}
image: "{{ $root.image.repository }}:{{ $root.image.tag | default $.Chart.AppVersion }}"
imagePullPolicy: {{ $root.image.pullPolicy }}
{{- end }}
