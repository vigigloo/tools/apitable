{{- define "apitable.shared.deploymentHead" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $root := (get $.Values $label) }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "apitable.nameBuilder" (list $ $label) }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
    app.kubernetes.io/part: {{ $label }}
spec:
  {{- if not $root.autoscaling.enabled }}
  replicas: {{ $root.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "common.selectorLabels" $ | nindent 6 }}
      app.kubernetes.io/part: {{ $label }}
  template:
    metadata:
      {{- with $root.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "common.selectorLabels" $ | nindent 8 }}
        app.kubernetes.io/part: {{ $label }}
    spec:
      {{- with $root.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      securityContext:
        {{- toYaml $root.podSecurityContext | nindent 8 }}
      {{- with $root.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with $root.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with $root.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
{{- end }}

