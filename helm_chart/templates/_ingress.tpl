{{- define "apitable.shared.ingressPath" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $path := (index . 2) }}
{{- $pathType := (index . 3) | default "Prefix" }}
{{- $root := (get $.Values $label) }}
{{- $name := include "apitable.nameBuilder" (list $ $label) }}
- path: {{ .path | default "" }}{{$path}}
  {{- if and $pathType (semverCompare ">=1.18-0" $.Capabilities.KubeVersion.GitVersion) }}
  pathType: {{ $pathType }}
  {{- end }}
  backend:
    {{- if semverCompare ">=1.19-0" $.Capabilities.KubeVersion.GitVersion }}
    service:
      name: {{ $name }}
      port:
        number: {{ $root.service.port }}
    {{- else }}
    serviceName: {{ $name }}
    servicePort: {{ $root.service.port }}
    {{- end }}
{{- end }}

