{{- define "apitable.shared.jobContainerAppData" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $jobName := (index . 2) }}
{{- $root := (get $.Values $label) }}
- name: {{ $jobName }}
  {{- include "apitable.shared.image" (list $ $label) | nindent 2 }}
  args: [{{ $jobName }}]
  env:
    {{- include "apitable.env.generic" $ | nindent 4 }}
    {{- include "apitable.env.redis" $ | nindent 4 }}
    {{- include "apitable.env.MySQL" $ | nindent 4 }}
    {{- include "apitable.env.rabbitMQ" $ | nindent 4 }}
    {{- include "apitable.env.S3" $ | nindent 4 }}
    - name: JAVA_OPTS
      value: {{ $root.javaOptions }}
  resources:
    {{- toYaml (get $root.containers $jobName).resources | nindent 4 }}
{{- end }}
