{{- define "apitable.env.generic" -}}
- name: ENV
  value: apitable
- name: TIMEZONE
  value: {{ .Values.timezone | quote }}
- name: TZ
  value: {{ .Values.timezone | quote }}
- name: DISABLE_AWSC
  value: "true"
- name: USE_CUSTOM_PUBLIC_FILES
  value: "true"
- name: ASSETS_BUCKET
  value: {{.Values.s3.assetsBucket | quote}}
- name: ASSETS_URL
  value: {{.Values.s3.assetsURL | quote}}
{{- end -}}

{{- define "apitable.env.template" -}}
- name: TEMPLATE_PATH
  value: ./static/web_build/index.html
- name: TEMPLATE_SPACE
  value: spcNTxlv8Drra
{{- end -}}

{{- define "apitable.env.MySQL" -}}
{{/*- name: NODE_TLS_REJECT_UNAUTHORIZED*/}}
{{/*  value: "0"*/}}
- name: MYSQL_DATABASE
  value: {{.Values.database.name | quote}}
- name: MYSQL_HOST
  value: {{.Values.database.host | quote}}
- name: MYSQL_PASSWORD
  value: {{.Values.database.password | quote}}
- name: MYSQL_PORT
  value: {{.Values.database.port | quote}}
- name: MYSQL_USERNAME
  value: {{.Values.database.username | quote}}
- name: DATABASE_TABLE_PREFIX
  value: {{.Values.database.prefix | quote}}
{{- end -}}

{{- define "apitable.env.redis" -}}
- name: REDIS_DB
  value: {{.Values.redis.database | quote}}
- name: REDIS_HOST
  value: {{.Values.redis.host | quote}}
- name: REDIS_PASSWORD
  value: {{.Values.redis.password | quote}}
- name: REDIS_PORT
  value: {{.Values.redis.port | quote}}
{{- end -}}

{{- define "apitable.env.rabbitMQ" -}}
- name: RABBITMQ_HOST
  value: {{.Values.rabbitmq.host | quote}}
- name: RABBITMQ_USERNAME
  value: {{.Values.rabbitmq.username | quote}}
- name: RABBITMQ_PASSWORD
  value: {{.Values.rabbitmq.password | quote}}
- name: RABBITMQ_PORT
  value: {{.Values.rabbitmq.port | quote}}
- name: RABBITMQ_VHOST
  value: /
{{- end -}}

{{- define "apitable.env.S3" -}}
- name: AWS_ACCESS_KEY
  value: {{.Values.s3.accessKey | quote}}
- name: AWS_ACCESS_SECRET
  value: {{.Values.s3.accessSecret | quote}}
- name: AWS_ENDPOINT
  value: {{.Values.s3.endpoint | quote}}
- name: AWS_REGION
  value: {{.Values.s3.region | quote}}
- name: OSS_ENABLED
  value: "true"
{{- end -}}

{{- define "apitable.env.socket" -}}
- name: ENABLE_SOCKET
  value: "true"
- name: SOCKET_RECONNECTION_ATTEMPTS
  value: "10"
- name: SOCKET_RECONNECTION_DELAY
  value: "500"
- name: SOCKET_TIMEOUT
  value: "5000"
{{- end -}}

{{- define "apitable.env.services" -}}
- name: SOCKET_URL
  value: http://{{ include "apitable.nameBuilder" (list . "room") }}:{{.Values.room.service.port.socket}}
- name: NEST_GRPC_ADDRESS
  value: static://{{ include "apitable.nameBuilder" (list . "room") }}:{{.Values.room.service.port.nestGRPC}}
- name: SOCKET_DOMAIN
  value: http://{{ include "apitable.nameBuilder" (list . "room") }}:{{.Values.room.service.port.room}}/socket
- name: API_PROXY
  value: http://{{ include "apitable.nameBuilder" (list . "backend") }}:{{.Values.backend.service.port}}
- name: BACKEND_BASE_URL
  value: http://{{ include "apitable.nameBuilder" (list . "backend") }}:{{.Values.backend.service.port}}/api/v1/
{{- end -}}

{{- define "apitable.env.email" -}}
- name: MAIL_ENABLED
  value: {{ .Values.email.enabled | quote }}
- name: MAIL_HOST
  value: {{ .Values.email.host | quote }}
- name: MAIL_USERNAME
  value: {{ .Values.email.username | quote }}
- name: MAIL_PASSWORD
  value: {{ .Values.email.password | quote }}
- name: MAIL_PORT
  value: {{ .Values.email.port | quote }}
- name: MAIL_SSL_ENABLE
  value: {{ .Values.email.ssl_enable | quote }}
- name: MAIL_TYPE
  value: {{ .Values.email.type | quote }}
{{- end -}}
