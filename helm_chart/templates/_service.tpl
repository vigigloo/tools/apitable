{{- define "apitable.shared.service" -}}
{{- $ := (first .) }}
{{- $label := (index . 1) }}
{{- $root := (get $.Values $label) }}
apiVersion: v1
kind: Service
metadata:
  name: {{ include "apitable.nameBuilder" (list $ $label) }}
  labels:
    {{- include "common.labels" $ | nindent 4 }}
    app.kubernetes.io/part: {{ $label }}
spec:
  type: {{ $root.service.type }}
  selector:
    {{- include "common.selectorLabels" $ | nindent 4 }}
    app.kubernetes.io/part: {{ $label }}
{{- end }}
